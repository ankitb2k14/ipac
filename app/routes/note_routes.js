var ObjectID = require('mongodb').ObjectID;
const multer = require('multer');
const path = require('path');

//Set Storage
const storage = multer.diskStorage({
	destination: './public/uploads/',
	filename: function(req, file, cb){
		cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
	}
});

//Init Upload
const upload = multer({
	storage: storage,
	limits: {
		fileSize: 1000000
	},
	fileFilter: function(req, file, cb){
		checkFileType(file, cb);
	}
}).single('file');

//check File Type
function checkFileType(file, cb){
	const filetypes = /jpeg|jpg|png|gif/;
	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
	const mimetype = filetypes.test(file.mimetype);

	if(mimetype && extname){
		return cb(null, true);
	}else{
		cb('Error: Images Only');
	}
}

module.exports = function(app, db){

	app.post('/register', (req, res) => {
		db.collection('register').insert(req.body, (err, result) => {
			if(err){
				res.status(400).send(`Please try after sometime ${err}`);
			}else{
				res.send({message: 'User has been successfully Registered ', email : result.ops[0].email});
			}
		});
	});

	app.get('/allUsers', (req, res) => {
		db.collection('register').find().toArray().then(function(response){
			res.send(response);
		});
	});

	app.post('/login', (req, res) => {
		db.collection('register').findOne(req.body, (err, item) => {
			if(err){
				res.status(400).send(`Please try after sometime ${err}`);
			}else{
				if(item == null)
				  res.status(401).send({message: 'Invalid Credentials'});
				else
					res.status(200).send(item);
			}
		});
	});

	app.post('/reports', (req, res) => {
		// const email = { 'email': req.body.email, 'sharedEmail': req.body.email};
		const email = { $or : [ { "email" : req.body.email }, {"sharedEmail":req.body.email } ] };
		db.collection('reports').find(email).toArray().then(function(response){
			res.send(response);
		});
	});

	app.get('/reports/:id', (req, res) => {
		const id = req.params.id;
		const details = {'_id': new ObjectID(id)};
		db.collection('reports').findOne(details, (err, item) => {
			if(err){
				res.send({ 'error': 'An error has occured' });
			}else{
				res.status(200).send(item);
			}
		});
	});

	app.post('/saveReport', (req, res) => {
		db.collection('reports').insert(req.body, (err, result) => {
			if(err){
				res.status(400).send(`Please try after sometime ${err}`);
			}else{
				console.log(result);
				res.send({message: 'Report Has Been successfully Saved '});
			}
		});
	});

	function tags(body, callback){
		// var body = body;
		var array1 = [];
		body.tags.forEach(function(val, index){
			array1.push({
				email: '',
				purpose: body.purpose,
				description: body.description,
				dateCreation: body.dateCreation,
				selectedCity: body.selectedCity,
				barchart: body.barchart,
				piecheckedData: body.piecheckedData,
				tags: body.tags,
				sharedEmail: val.text,
				fileName: body.fileName
			});
			if(index+1 == body.tags.length){
				callback(array1);  
			}
		});
	}


	app.post('/shareReport', (req, res) => {	
		tags(req.body,function(arr){
			var search = arr.map(function(v){
   				 return v.sharedEmail;
			});	
			// console.log(arr)
			const email = { "email": { $in : search } };
			db.collection('register').find(email).toArray().then(function(response){

				if(response.length > 0){
					db.collection('reports').insert(arr, (err, result) => {
					if(err){
						res.status(400).send(`Please try after sometime ${err}`);
					}else{
						console.log(result);
						res.send({message: 'Report Has Been successfully Shared '});
					}
				});
				}else{
					res.status(400).send(`Please try after sometime`);
				}
			});
			
			// const email = { "email": { $in : search } };
			// db.collection('register').find(email).toArray().then(function(response){

				
			// });
		});
		
	});
	

	// app.get('/image', (req, res) => {
	// 	db.collection('Image').find().toArray().then(function(response){
	// 		console.log(response);
	// 		res.send(response);
	// 	});
	// });

	app.get('/notes/:id', (req, res) => {
		const id = req.params.id;
		const details = {'_id': new ObjectID(id)};
		db.collection('notes').findOne(details, (err, item) => {
			if(err){
				res.send({ 'error': 'An error has occured' });
			}else{
				res.send(item);
			}
		});
	});

	app.post('/notes', (req, res) => {
		const note = { text: req.body.body, title: req.body.title };
		db.collection('notes').insert(note, (err, result) => {
			if(err){
				res.send({ 'error': 'An error has occured' });
			}else{
				res.send(result.ops[0]);
			}
		});
	});

	app.put('/notes/:id', (req, res) => {
		const id = req.params.id;
		const details = {'_id': new ObjectID(id)};
		const note = { text: req.body.body, title: req.body.title };
		db.collection('notes').update(details, note , (err, item) => {
			if(err){
				res.send({ 'error': 'An error has occured' });
			}else{
				res.send(item);
			}
		});
	});

	app.delete('/notes/:id', (req, res) => {
		const id = req.params.id;
		const details = {'_id': new ObjectID(id)};
		db.collection('notes').remove(details, (err, item) => {
			if(err){
				res.send({ 'error': 'An error has occured' });
			}else{
				res.send('Note ' + id + ' deleted');
			}
		});
	});



	// app.post('/upload', (req, res) => {
	// 	upload(req, res, (err) => {
	// 		console.log(req.file);
	// 		if(err){
	// 			res.status(400).send('An error has occured');
	// 		}else{
				
	// 			if(req.file == undefined){
	// 				res.status(400).send('No file Send');
	// 			} else{

	// 				let file = { file: req.file.filename };
	// 				let sql = 'INSERT INTO images SET ?';
	// 				let query = db.query(sql, file, (err, result) => {
	// 					console.log(db);
	// 					if(err) 
	// 						res.status(400).send('Please try after sometime');
	// 					else
	// 						res.status(200).send(result);
	// 				});
	// 			}
	// 		}
	// 	});
	// });

	// app.get('/image', (req, res) => {
	// 	let sql = 'SELECT * FROM images';
	// 	let query = db.query(sql , (err, result) => {	
	// 		if(err) 
	// 			res.status(400).send(`Please try after sometime ${err}`);
	// 		else
	// 			res.status(200).send(result);
	// 	});
	// });

	// app.delete('/image/:id', (req, res) => {
	// 	let sql = `DELETE FROM images WHERE id = ${req.params.id}`;
	// 	let query = db.query(sql , (err, result) => {
	// 		if(err) 
	// 			res.status(400).send('Please try after sometime');
	// 		else
	// 			res.status(200).send(`Image ${req.params.id} has been deleted`);
	// 	});
	// });
};