const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const ejs = require('ejs');
const db = require('./config/db'); 
// const mysql = require('mysql');

//Init app
const app = express(); 

app.use(bodyParser.json());
//EJS
app.set('view engine', 'ejs');

//Public Folder
app.use(express.static(__dirname + '/public'));

const port = process.env.PORT || 3000;

MongoClient.connect(db.url, (err, database) => {
	if(err) return console.log(err);

	const dbBase = database.db('ankit-ipac')
	require('./app/routes')(app, dbBase);
	app.listen(port,function(){
		console.log(`Server is listening at port ${port}`)
	})
});







