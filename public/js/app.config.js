angular.module('IPACModule')
	   .config(['$stateProvider', '$urlRouterProvider' , function($stateProvider, $urlRouterProvider){

	$stateProvider
		.state('landing', {
			url: '/',
			templateUrl: 'views/landing.html'
		})
		.state('dataAnalytics', {
			url: '/dataAnalytics&Tech',
			templateUrl: 'views/dataAnalytics.html'
		})
		.state('hr', {
			url: '/hr',
			templateUrl: 'views/hr.html'
		})
		.state('socialMedia', {
			url: '/socialMedia',
			templateUrl: 'views/socialMedia.html'
		})
		.state('media', {
			url: '/media',
			templateUrl: 'views/media.html',
			controller: 'MediaController'
		})
		.state('mediareportMaking', {
			url: '/reportMaking',
			templateUrl: 'views/mediaReport.html',
			controller: 'ReportMakingController'
		})
		.state('mediareportMakingId', {
			url: '/reportMaking/:id',
			templateUrl: 'views/mediaReport.html',
			controller: 'ReportMakingController'
		})
		.state('finance', {
			url: '/finance',
			templateUrl: 'views/finance.html'
		})
		.state('leadership', {
			url: '/leadership',
			templateUrl: 'views/leadership.html',
			controller : 'LeadershipController'
		});
		$urlRouterProvider.when('', '/');
}]);