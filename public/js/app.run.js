angular.module('IPACModule')
	   .run(function($localStorage, $rootScope, $state, $location){
	   		if($localStorage.loginDetails){
	   			$rootScope.loginDetails = $localStorage.loginDetails;
	   		}else{
	   			$location.path('/')
	   		}
	   });	