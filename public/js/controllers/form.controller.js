angular.module('IPACModule')
	   .controller('FormController', formController);

	   formController.$inject = [
	   	'$scope',
	   	'CommonService',
	   	'toaster',
	   	'$rootScope',
	   	'$state',
	   	'$localStorage'
	   ];

	   function formController(
	   	$scope,
	   	CommonService,
	   	toaster,
	   	$rootScope,
	   	$state,
	   	$localStorage
	   	){
	   	console.log('In formController')

	   	//Initializing scope variables
	   	$scope.form = {
	   		invalidLogin: false,
	   		showRegisterForm: false,
	   		roles: CommonService.roles(),
	   		registerForm: registerForm,
	   		resetform: resetform
	   	}

	   	$scope.register = {
	   		firstName: '',
	   		lastName: '',
	   		email: '',
	   		password: '',
	   		role: ''
	   	};

	   	$scope.register.role = $scope.form.roles[0].id;
	   	
	   	$scope.formValues = {
	   		email: '',
	   		password: ''
	   	}

	   	function resetform(){
	   		$scope.form.invalidLogin = false;
	   			$scope.formValues = {
			   		email: '',
			   		password: ''
	   			}
	   	}

	    $scope.submitForm = function(loginValues){
	    	if($state.current.name == 'media'){
	    		$state.reload();
	    		$rootScope.reportsArray = [];
	    		$scope.logInSpinner = true;
	    		CommonService.loginForm(loginValues).then(function(loginResponse){
	   			if(loginResponse.status === 200){
	   				$scope.logInSpinner = false;
	   		 		$rootScope.loginDetails = loginResponse.data;
	   		 		$localStorage.loginDetails = loginResponse.data;
	   		 		toaster.pop('success', 'Welcome ' + loginResponse.data.email);
	   		 		CommonService.getReports($rootScope.loginDetails).then(function(reportsResponse){
		 				console.log(reportsResponse,'reportsResponse');
		 				$rootScope.reportsArray = reportsResponse.data;
 					    $rootScope.loadSpinner = false;
		 			},function(error){
		 				console.log(error,'error')
		 			});
	   		 	}
	   		},function(error){
	   			if(error === 'Invalid Credentials'){
	   				$scope.form.invalidLogin = true;
	   			}
	   		});
	      }else{
	      	toaster.pop('note', 'Please visit media Page');
	      }
	   	}

	   	$scope.submitRegister = function(registerValues){
	   		if($state.current.name == 'media'){
		   		$scope.registerSpinner = true;
		   		CommonService.registerForm(registerValues).then(function(registerResponse){
		   			if(registerResponse.status === 200){
		   				$scope.registerSpinner = false;
		   		 		toaster.pop('success', registerResponse.data.email , registerResponse.data.message);
		   		 		$scope.form.showRegisterForm = false;
		   			}

		   		},function(error){
		   			console.log(error);
		   		});
	   		}else{
	      	toaster.pop('note', 'Please visit media Page');
	      }
	   	}

	   	function registerForm(){
	   		$scope.form.showRegisterForm = !$scope.form.showRegisterForm;
	   	}
   }
