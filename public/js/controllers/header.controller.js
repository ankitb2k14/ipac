angular.module('IPACModule')
	   .controller('HeaderController', headerController);

	   headerController.$inject = [
	   	'$scope',
	   	'$state',
	   	'$localStorage',
	   	'$rootScope'
	   ];

	   function headerController(
	   	$scope,
	   	$state,
	   	$localStorage,
	   	$rootScope){
	   	console.log('In headerController')

	   	//Initailzing scope variables
	   	$scope.header = {
	   		logout: logout
	   	}

	   	function logout(){
	   		$rootScope.loginDetails = '';
	   		$localStorage.$reset();
	   		$state.go('media');
	   	};
	   }