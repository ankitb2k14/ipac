angular.module('IPACModule')
	   .controller('MediaController', mediaController);

	   mediaController.$inject = [
	   	'$scope',
	   	'$state',
	   	'$rootScope',
	   	'CommonService',
	   	'$timeout'
	   ];

	   function mediaController(
	   	$scope,
	   	$state,
	   	$rootScope,
	   	CommonService,
	   	$timeout){
	   	console.log('In mediaController')

	   	//Initializing scope variables
	   	$scope.media = {
	   		createReport: createReport,
	   		openReport: openReport,
	   		showAllUsers: showAllUsers
	   	};

	   	$rootScope.loadSpinner = true;

	   	function createReport(){
	   		$state.go('mediareportMaking')
	   	}

	   	function openReport(report){
	   		$state.go('mediareportMakingId', {id: report['_id']});
	   	}

	   	function init(){	   		
	   		CommonService.getReports($rootScope.loginDetails).then(function(reportsResponse){
 				console.log(reportsResponse,'reportsResponse');
 				if(reportsResponse.status === 200){
 					$scope.reportsArray = reportsResponse.data;
 					$scope.loadSpinner = false;
 				}
 			},function(error){
 				console.log(error,'error')
 			});
	   	}
	   	if($rootScope.loginDetails)
	   		init();

	   	function showAllUsers(){
	   		CommonService.allusers().then(function(response){

	   			$scope.allusers = response.data;
	   		});
	   	}
	   	$timeout(function(){
	   		if($rootScope.loginDetails.role == 1 || $rootScope.loginDetails.role == 2){
	   		showAllUsers();
	   	}
	   	},1000);
	   	
	   }