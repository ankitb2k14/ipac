angular.module('IPACModule')
	   .controller('ReportMakingController', reportMakingController);

	   reportMakingController.$inject = [
	   	'$scope',
	   	'$state',
	   	'CommonService',
	   	'$rootScope',
	   	'$uibModal',
        '$stateParams',
        'toaster'
	   ];

	   function reportMakingController(
	   	$scope,
	   	$state,
	   	CommonService,
	   	$rootScope,
	   	$uibModal,
        $stateParams,
        toaster){
	   	console.log('In reportMakingController')
        $scope.tags = [];
 
	   	//Inializing scope variables
	   	$scope.report = {
	   		barchartOptions: CommonService.barchartOptions(),
            pieDataOptions: CommonService.pieDataOptions(),
            CitiesDetail: CommonService.CitiesDetail(),
	   		barchartData: barchartData,
	   		openModal:openModal,
	   		downloadReport: downloadReport,
            piechart: piechart,
            shareReport: shareReport
	   	};

        $scope.generalInfo = {

        }
        $scope.popup1 = {
            opened: false
        };

	   	 $scope.open1 = function() {
            $scope.popup1.opened = true;
         };

        $scope.reportModel = {
	   		email: $rootScope.loginDetails.email
	   	}

        $scope.type=[$scope.reportModel.piecheckedData];

        $scope.saved = false;
        if(!$stateParams.id){
        	barChart('data/data.json');
            $scope.reportModel.barchart = $scope.report.barchartOptions[0];
            $scope.reportModel.piecheckedData=$scope.report.pieDataOptions[0];
            $scope.reportModel.selectedCity= $scope.report.CitiesDetail[0];
            initMap($scope.reportModel.selectedCity.lat,$scope.reportModel.selectedCity.long);
        }else{
        	getreportById($stateParams.id);
        }

        function getreportById(id){
            CommonService.getReportById(id).then(function(reportResponse){
                $scope.reportModel = reportResponse.data;
                var reportResponse = reportResponse.data;
                $scope.reportModel['purpose'] = reportResponse['purpose'];
                $scope.reportModel['description'] = reportResponse['description'];
                $scope.reportModel['dateCreation'] = new Date(reportResponse['dateCreation']);
                initMap(reportResponse.selectedCity.lat,reportResponse.selectedCity.long);
                $scope.reportModel.selectedCity = reportResponse.selectedCity;
                angular.forEach($scope.report.barchartOptions, function(val, key){
                    if(val.id == reportResponse['barchart']['id']){
                        $scope.reportModel.barchart = $scope.report.barchartOptions[key];
                        barchartData();
                    }
                });
                angular.forEach($scope.report.pieDataOptions, function(val, key){
                    if(val.id == reportResponse['piecheckedData']['id']){
                        $scope.type=[val.name];
                        $scope.reportModel.piecheckedData=$scope.report.pieDataOptions[key];
                         var donutData = window.genData($scope.type);
                         var donuts = new window.DonutCharts();
                         donuts.create(donutData);

                    }
                });
                 angular.forEach($scope.report.CitiesDetail, function(val, key){
                    if(val.city == reportResponse['selectedCity']['city']){
                       $scope.reportModel.selectedCity = $scope.report.CitiesDetail[key];
                       initMap(reportResponse.selectedCity.lat,reportResponse.selectedCity.long);
                    }
                });

            },function(error){
                console.log(error,'error')
            });
        }
        $scope.showShare = false;
        function shareReport(reportDetails){
            if($stateParams.id){
                 $scope.showShare = !$scope.showShare;
                console.log($scope.reportModel);
                if($scope.reportModel.tags ){
                    $scope.showShare = true;
                     CommonService.shareReport($scope.reportModel).then(function(shareResponse){
                        toaster.pop('success', shareResponse.data.message);

                    },function(err){
                        toaster.pop('warning', 'Something went wrong');
                    });
                }
            }else{
                 toaster.pop('warning','First Save the file!');
            }
           
           
        }

        function barchartData(){
            console.log($scope.reportModel.barchart);
            if($scope.reportModel.barchart.id == $scope.report.barchartOptions[0].id){
                barChart('data/data.json')
            }else if($scope.reportModel.barchart.id == $scope.report.barchartOptions[1].id){
                barChart('data/data1.json')
            }else if($scope.reportModel.barchart.id == $scope.report.barchartOptions[2].id){
                barChart('data/data2.json')
            }else if($scope.reportModel.barchart.id == $scope.report.barchartOptions[3].id){
                barChart('data/data4.json')
            }
        }
     
        function openModal(){
        	var modalInstance = $uibModal.open({
        		template: `<div class="modal-header">
					            <h3 class="modal-title" id="modal-title">Please Input File Name.</h3>
					        </div>
					        <div class="modal-body" id="modal-body">
					            <form name="form">        
							           <div class="form-group row">
							              <div class="col-sm-9">
							                	<input type="text" class="form-control" ng-model="modal.fileName" name="fileName" required/>                 
							              </div>
							              <div class="help-block" ng-if="form.fileName.$invalid && !form.fileName.$pristine">
                            				This Field is Required.
                          				  </div>
							           </div>
							        </form>
					        </div>
					        <div class="modal-footer">
					            <button class="btn btn-primary" type="button" ng-click="save(modal);" ng-disabled="form.$invalid">Save</button>
					            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
					        </div>`,
        		controller: function($scope, $uibModalInstance){
        			console.log('In Modal controller');
        			$scope.modal = {
        				fileName: ''
        			}
        			$scope.save = function(modal){
        				$uibModalInstance.close(modal)
        			};
        			$scope.cancel = function(){
        				$uibModalInstance.dismiss();
        			}
        		}
        	})

        	modalInstance.result.then(function (modal) {
        		if(modal){
        			angular.extend($scope.reportModel, modal);
        			saveReport($scope.reportModel);
        		}
		    }, function (error) {
		    	console.log(error);
		    });
        }
        function saveReport(reportDetails){
            // var date =  new Date(reportDetails.dateCreation).toISOString();
            // reportDetails.dateCreation = date.substr(0,10);
            $scope.saved = true;
        	console.log(reportDetails)
            if(reportDetails['_id']){
                delete reportDetails['_id'];
            }
        	CommonService.saveReport(reportDetails).then(function(reportResponse){
        		if(reportResponse.status === 200){
        			console.log(reportResponse);
	   		 		toaster.pop('success', reportResponse.data.message);
        		}
        	},function(err){
                toaster.pop('warning', 'Something went wrong');
        	});
        }

        function downloadReport(){
        	console.log('downloadReport')


        }

        var doc = new jsPDF();
			var specialElementHandlers = {
			    '#editor': function (element, renderer) {
			        return true;
			    }
			};

			$('#cmd').click(function () {
				alert();
			    doc.fromHTML($('#reportPdf').html(), 15, 15, {
			        'width': 170,
			            'elementHandlers': specialElementHandlers
			    });
			    doc.save('sample-file.pdf');
			});

          function barChart(Url){
               $('#barChart').empty()
               var w= $('#barChart').width();
               var h=$('#barChart').height();
                var margin = {top: 20, right: 20, bottom: 30, left: 40},
                width = w - margin.left - margin.right,
                height = h - margin.top - margin.bottom;

                var x0 = d3.scale.ordinal()
                .rangeRoundBands([0, width], .1);

                var x1 = d3.scale.ordinal();

                var y = d3.scale.linear()
                .range([height, 0]);

                var xAxis = d3.svg.axis()
                .scale(x0)
                .tickSize(0)
                .orient("bottom");

                var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

                var color = d3.scale.ordinal()
                .range(["#ca0020","#f4a582","#d5d5d5","#92c5de","#0571b0"]);

                var svg = d3.select('#barChart').append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                d3.json(Url, function(error, data) {

                var categoriesNames = data.map(function(d) { return d.categorie; });
                var rateNames = data[0].values.map(function(d) { return d.rate; });

                x0.domain(categoriesNames);
                x1.domain(rateNames).rangeRoundBands([0, x0.rangeBand()]);
                y.domain([0, d3.max(data, function(categorie) { return d3.max(categorie.values, function(d) { return d.value; }); })]);

                svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

                svg.append("g")
                .attr("class", "y axis")
                .style('opacity','0')
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .style('font-weight','bold')
                .text("Value");

                svg.select('.y').transition().duration(500).delay(1300).style('opacity','1');

                var slice = svg.selectAll(".slice")
                .data(data)
                .enter().append("g")
                .attr("class", "g")
                .attr("transform",function(d) { return "translate(" + x0(d.categorie) + ",0)"; });

                slice.selectAll("rect")
                .data(function(d) { return d.values; })
                .enter().append("rect")
                .attr("width", x1.rangeBand())
                .attr("x", function(d) { return x1(d.rate); })
                .style("fill", function(d) { return color(d.rate) })
                .attr("y", function(d) { return y(0); })
                .attr("height", function(d) { return height - y(0); })
                .on("mouseover", function(d) {
                    d3.select(this).style("fill", d3.rgb(color(d.rate)).darker(2));
                })
                .on("mouseout", function(d) {
                    d3.select(this).style("fill", color(d.rate));
                });

                slice.selectAll("rect")
                .transition()
                .delay(function (d) {return Math.random()*1000;})
                .duration(1000)
                .attr("y", function(d) { return y(d.value); })
                .attr("height", function(d) { return height - y(d.value); });

                //Legend
                var legend = svg.selectAll(".legend")
                .data(data[0].values.map(function(d) { return d.rate; }).reverse())
                .enter().append("g")
                .attr("class", "legend")
                .attr("transform", function(d,i) { return "translate(0," + i * 20 + ")"; })
                .style("opacity","0");

                legend.append("rect")
                .attr("x", width - 18)
                .attr("width", 18)
                .attr("height", 18)
                .style("fill", function(d) { return color(d); });

                legend.append("text")
                .attr("x", width - 24)
                .attr("y", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .text(function(d) {return d; });

                legend.transition().duration(500).delay(function(d,i){ return 1300 + 100 * i; }).style("opacity","1");

                });

            }
                

            $(function() {
                var donutData = window.genData($scope.type);
                var donuts = new window.DonutCharts();
        
                    donuts.create(donutData);
        
                $('#refresh-btn').on('click', function refresh() {
                    donuts.update(window.genData);
                });
        
            });
            
        
            window.DonutCharts = function() {
        
                var charts = d3.select('#pieChart');
        
                var chart_m,
                    chart_r,
                    color = d3.scale.category20();
        
                var getCatNames = function(dataset) {
                    var catNames = new Array();
        
                    for (var i = 0; i < dataset[0].data.length; i++) {
                        catNames.push(dataset[0].data[i].cat);
                    }
        
                    return catNames;
                }
        
                var createLegend = function(catNames) {
                    var legends = charts.select('.legend')
                                    .selectAll('g')
                                        .data(catNames)
                                    .enter().append('g')
                                        .attr('transform', function(d, i) {
                                            return 'translate(' + (i * 150 + 50) + ', 10)';
                                        });
            
                    legends.append('circle')
                        .attr('class', 'legend-icon')
                        .attr('r', 6)
                        .style('fill', function(d, i) {
                            return color(i);
                        });
            
                    legends.append('text')
                        .attr('dx', '1em')
                        .attr('dy', '.3em')
                        .text(function(d) {
                            return d;
                        });
                }
        
                var createCenter = function(pie) {
        
                    var eventObj = {
                        'mouseover': function(d, i) {
                            d3.select(this)
                                .transition()
                                .attr("r", chart_r * 0.65);
                        },
        
                        'mouseout': function(d, i) {
                            d3.select(this)
                                .transition()
                                .duration(500)
                                .ease('bounce')
                                .attr("r", chart_r * 0.6);
                        },
        
                        'click': function(d, i) {
                            var paths = charts.selectAll('.clicked');
                            pathAnim(paths, 0);
                            paths.classed('clicked', false);
                            resetAllCenterText();
                        }
                    }
        
                    var donuts = d3.selectAll('.donut');
        
                    // The circle displaying total data.
                    donuts.append("svg:circle")
                        .attr("r", chart_r * 0.6)
                        .style("fill", "#E7E7E7")
                        .on(eventObj);
            
                    donuts.append('text')
                            .attr('class', 'center-txt type')
                            .attr('y', chart_r * -0.16)
                            .attr('text-anchor', 'middle')
                            .style('font-weight', 'bold')
                            .text(function(d, i) {
                                return d.type;
                            });
                    donuts.append('text')
                            .attr('class', 'center-txt value')
                            .attr('text-anchor', 'middle');
                    donuts.append('text')
                            .attr('class', 'center-txt percentage')
                            .attr('y', chart_r * 0.16)
                            .attr('text-anchor', 'middle')
                            .style('fill', '#A2A2A2');
                }
        
                var setCenterText = function(thisDonut) {
                    var sum = d3.sum(thisDonut.selectAll('.clicked').data(), function(d) {
                        return d.data.val;
                    });
        
                    thisDonut.select('.value')
                        .text(function(d) {
                            return (sum)? sum.toFixed(1) + d.unit
                                        : d.total.toFixed(1) + d.unit;
                        });
                    thisDonut.select('.percentage')
                        .text(function(d) {
                            return (sum)? (sum/d.total*100).toFixed(2) + '%'
                                        : '';
                        });
                }
        
                var resetAllCenterText = function() {
                    charts.selectAll('.value')
                        .text(function(d) {
                            return d.total.toFixed(1) + d.unit;
                        });
                    charts.selectAll('.percentage')
                        .text('');
                }
        
                var pathAnim = function(path, dir) {
                    switch(dir) {
                        case 0:
                            path.transition()
                                .duration(500)
                                .ease('bounce')
                                .attr('d', d3.svg.arc()
                                    .innerRadius(chart_r * 0.7)
                                    .outerRadius(chart_r)
                                );
                            break;
        
                        case 1:
                            path.transition()
                                .attr('d', d3.svg.arc()
                                    .innerRadius(chart_r * 0.7)
                                    .outerRadius(chart_r * 1.08)
                                );
                            break;
                    }
                }
        
                var updateDonut = function() {
        
                    var eventObj = {
        
                        'mouseover': function(d, i, j) {
                            pathAnim(d3.select(this), 1);
        
                            var thisDonut = charts.select('.type' + j);
                            thisDonut.select('.value').text(function(donut_d) {
                                return d.data.val.toFixed(1) + donut_d.unit;
                            });
                            thisDonut.select('.percentage').text(function(donut_d) {
                                return (d.data.val/donut_d.total*100).toFixed(2) + '%';
                            });
                        },
                        
                        'mouseout': function(d, i, j) {
                            var thisPath = d3.select(this);
                            if (!thisPath.classed('clicked')) {
                                pathAnim(thisPath, 0);
                            }
                            var thisDonut = charts.select('.type' + j);
                            setCenterText(thisDonut);
                        },
        
                        'click': function(d, i, j) {
                            var thisDonut = charts.select('.type' + j);
        
                            if (0 === thisDonut.selectAll('.clicked')[0].length) {
                                thisDonut.select('circle').on('click')();
                            }
        
                            var thisPath = d3.select(this);
                            var clicked = thisPath.classed('clicked');
                            pathAnim(thisPath, ~~(!clicked));
                            thisPath.classed('clicked', !clicked);
        
                            setCenterText(thisDonut);
                        }
                    };
        
                    var pie = d3.layout.pie()
                                    .sort(null)
                                    .value(function(d) {
                                        return d.val;
                                    });
        
                    var arc = d3.svg.arc()
                                    .innerRadius(chart_r * 0.7)
                                    .outerRadius(function() {
                                        return (d3.select(this).classed('clicked'))? chart_r * 1.08
                                                                                   : chart_r;
                                    });
        
                    // Start joining data with paths
                    var paths = charts.selectAll('.donut')
                                    .selectAll('path')
                                    .data(function(d, i) {
                                        return pie(d.data);
                                    });
        
                    paths
                        .transition()
                        .duration(1000)
                        .attr('d', arc);
        
                    paths.enter()
                        .append('svg:path')
                            .attr('d', arc)
                            .style('fill', function(d, i) {
                                return color(i);
                            })
                            .style('stroke', '#FFFFFF')
                            .on(eventObj)
        
                    paths.exit().remove();
        
                    resetAllCenterText();
                }
        
                this.create = function(dataset) {
                    var $charts = $('#pieChart');
                    chart_m = $charts.innerWidth() / dataset.length / 2 * 0.14;
                    chart_r = $charts.innerWidth() / dataset.length / 2 * 0.85;
        
                    charts.append('svg')
                        .attr('class', 'legend')
                        .attr('width', '100%')
                        .attr('height', 50)
                        .attr('transform', 'translate(0, -100)');
        
                    var donut = charts.selectAll('.donut')
                                    .data(dataset)
                                .enter().append('svg:svg')
                                    .attr('width', (chart_r + chart_m) * 2)
                                    .attr('height', (chart_r + chart_m) * 2)
                                .append('svg:g')
                                    .attr('class', function(d, i) {
                                        return 'donut type' + i;
                                    })
                                    .attr('transform', 'translate(' + (chart_r+chart_m) + ',' + (chart_r+chart_m) + ')');
        
                    createLegend(getCatNames(dataset));
                    createCenter();
        
                    updateDonut();
                }
            
                this.update = function(dataset) {
                    // Assume no new categ of data enter
                    var donut = charts.selectAll(".donut")
                                .data(dataset);
        
                    updateDonut();
                }
            }
        
        
            /*
             * Returns a json-like object.
             */
            window.genData = function(type) {
                var type =$scope.type;
                var unit = ['M', 'GB', ''];
                var cat = ['Google Drive', 'Dropbox', 'iCloud'];
        
                var dataset = new Array();
        
                for (var i = 0; i < type.length; i++) {
                    var data = new Array();
                    var total = 0;
        
                    for (var j = 0; j < cat.length; j++) {
                        var value = Math.random()*10*(3-i);
                        total += value;
                        data.push({
                            "cat": cat[j],
                            "val": value
                        });
                    }
        
                    dataset.push({
                        "type": type[i],
                        "unit": unit[i],
                        "data": data,
                        "total": total
                    });
                }
                $scope.pieUpdatedData=dataset;
                return dataset;
            }

           
                function piechart(piecheckedData){
                    $('#pieChart').empty();
                    if(piecheckedData.id == $scope.report.pieDataOptions[0].id){
                        $scope.type = [$scope.report.pieDataOptions[0].name];
                    }else  if(piecheckedData.id == $scope.report.pieDataOptions[1].id){
                        $scope.type = [$scope.report.pieDataOptions[1].name];
                    }else if(piecheckedData.id == $scope.report.pieDataOptions[2].id){
                        $scope.type = [$scope.report.pieDataOptions[2].name];  
                    }
                    var donutData = window.genData($scope.type);
                    var donuts = new window.DonutCharts();
                    donuts.create(donutData);
                }
                $scope.map=function(){
                    $('#map').empty();
                    console.log($scope.reportModel.selectedCity,"selectedCity")
                    initMap($scope.reportModel.selectedCity.lat,$scope.reportModel.selectedCity.long);
                }
              

                function initMap(latitude,longitude) {
                    var uluru = {lat:latitude, lng:longitude};
                    var map = new google.maps.Map(document.getElementById('map'), {
                      zoom: 8,
                      center: uluru
                    });
                    var marker = new google.maps.Marker({
                      position: uluru,
                      map: map
                    });
                  }



	   }