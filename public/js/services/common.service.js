angular.module('IPACModule')
	   .factory('CommonService', commonService);

	   commonService.$inject = [
	   	 '$http',
	   	 '$q'
	   ];

	   function commonService(
	   	$http,
	   	$q){
	   	  return {
	   	  	  roles: roles,
	   	  	  loginForm: loginForm,
	   	  	  registerForm: registerForm,
	   	  	  getReports: getReports,
	   	  	  barchartOptions: barchartOptions,
	   	  	  saveReport: saveReport,
	   	  	  pieDataOptions: pieDataOptions,
	   	  	  getReportById: getReportById,
	   	  	  CitiesDetail: CitiesDetail,
	   	  	  shareReport: shareReport,
	   	  	  allusers:allusers
	   	  }

	   	  function roles(){
	   	  	return [
	   	  		{id: 1, name: 'Director'},
	   	  		{id: 2, name: 'Senior Associate'},
	   	  		{id: 3, name: 'Associate'},
	   	  		{id: 4, name: 'Junior Associate'}
	   	  	];
	   	  }

	   	  function barchartOptions(){
	   	  	return [
	   	  		{id: 1, name: 'option1'},
	   	  		{id: 2, name: 'option2'},
	   	  		{id: 3, name: 'option3'},
	   	  		{id: 4, name: 'option4'}
	   	  	];
	   	  }

	   	  function pieDataOptions(){
	   	  	return [
	   	  		{id: 1, name: 'Users'},
	   	  		{id: 2, name: 'Avg Upload'},
	   	  		{id: 3, name: 'Avg Files Shared'}
	   	  	];
	   	  }

	   	  function CitiesDetail(){
	   	  	return [{
                	city:'Orai, Uttar Pradesh, India',
                	lat:25.989836,
                	long:79.450035
                },
                {
                	city:'Himmatnagar, Gujarat, India',
                	lat:23.597969,
                	long:72.969818
                },
                {
                	city:'Dhanpuri, Madhya Pradesh, India',
                	lat:23.173939,
                	long:81.565125
                },
                {
                	city:'Ausa, Maharashtra, India',
                	lat:18.245655,
                	long:76.505356
                },
                {
                	city:'Pali, Rajasthan, India',
                	lat:25.771315,
                	long:73.323685
              }];
	   	  }
	   	  
	   	  function loginForm(data){
	   	  	var apiUrl =  '/login';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
                data: data
	        });
	        return(request
	                .then(loginFormSuccess)
	                .catch(loginFormError));
	
	        /*loginForm error function*/
	        function loginFormError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*loginForm success function*/
	        function loginFormSuccess(response) {
	            return(response);
	        }
	   	  }

	   	  function registerForm(data){
	   	  	var apiUrl =  '/register';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
                data: data
	        });
	        return(request
	                .then(registerFormSuccess)
	                .catch(registerFormError));
	
	        /*registerForm error function*/
	        function registerFormError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*registerForm success function*/
	        function registerFormSuccess(response) {
	            return(response);
	        }
	   	  }

	   	  function getReports(data){
	   	  	var apiUrl =  '/reports';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data
	        });
	        return(request
	                .then(getReportsSuccess)
	                .catch(getReportsError));
	
	        /*getReports error function*/
	        function getReportsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getReports success function*/
	        function getReportsSuccess(response) {
	            return(response);
	        }
	   	  }

	   	  function saveReport(data){
	   	  	var apiUrl =  '/saveReport';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data
	        });
	        return(request
	                .then(saveReportSuccess)
	                .catch(saveReportError));
	
	        /*saveReport error function*/
	        function saveReportError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*saveReport success function*/
	        function saveReportSuccess(response) {
	            return(response);
	        }
	   	  }

	   	  function getReportById(id){
	   	  	var apiUrl =  '/reports/' + id;
			var request = $http({
	            method: "GET",
	            url: apiUrl
	        });
	        return(request
	                .then(getReportByIdSuccess)
	                .catch(getReportByIdError));
	
	        /*getReportById error function*/
	        function getReportByIdError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getReportById success function*/
	        function getReportByIdSuccess(response) {
	            return(response);
	        }
	   	  }

	   	  function shareReport(data){
	   	  	var apiUrl =  '/shareReport';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data
	        });
	        return(request
	                .then(shareReportSuccess)
	                .catch(shareReportError));
	
	        /*shareReport error function*/
	        function shareReportError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*shareReport success function*/
	        function shareReportSuccess(response) {
	            return(response);
	        }
	   	  }

	   	  function allusers(){
	   	  	var apiUrl =  '/allUsers';
			var request = $http({
	            method: "GET",
	            url: apiUrl
	        });
	        return(request
	                .then(allusersSuccess)
	                .catch(allusersError));
	
	        /*allusers error function*/
	        function allusersError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*allusers success function*/
	        function allusersSuccess(response) {
	            return(response);
	        }
	   	  }
	   	  
	   }